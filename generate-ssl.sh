#!/bin/bash

set -e

CURRENT_DATE=$(date +"%Y-%m-%d-%H-%M-%S")
CERT_FILE=${CURRENT_DATE}/fullchain.cer
KEY_FILE=${CURRENT_DATE}/${DOMAIN}.key

echo "Generating Certificates"
/root/.acme.sh/acme.sh --issue --dns dns_dgon -d "${DOMAIN}" -d "*.${DOMAIN}" --yes-I-know-dns-manual-mode-enough-go-ahead-please --server letsencrypt

echo "Moving certificates to dated folder ${CURRENT_DATE}"
mkdir "${CURRENT_DATE}"
cp -vr ~/.acme.sh/smc.org.in/* "${CURRENT_DATE}"

echo "Copying certificates to server"
scp -r "${CURRENT_DATE}" "${SERVER_ACCESS}":~/.certificates/
scp -r "${CURRENT_DATE}"/* "${SERVER_ACCESS}":~/.certificates/latest/
echo "Reloading nginx on the server for certificates to take effect"
ssh "${SERVER_ACCESS}" "sudo service nginx reload"

echo "Updating GitLab Pages certificates - smc.org.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/${WEBSITE_PROJECT_ID}/pages/domains/${DOMAIN}" > /dev/null

sleep 10

echo "Updating GitLab Pages certificates - swanalekha.smc.org.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/${SWANALEKHA_PROJECT_ID}/pages/domains/swanalekha.${DOMAIN}" > /dev/null

echo "Updating GitLab Pages certificates - planet.smc.org.in"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/${PLANET_PROJECT_ID}/pages/domains/planet.${DOMAIN}" > /dev/null
